# AppLSAC

This repository stores a collection of WebApps that run in a browser as runtime environment ([AppLSAC](https://en.wikiversity.orh/wiki/AppLSAC)).
The repository contains files for the learning resource about AppLSACs on Wikiversity, so that learners are enabled to create WebApps themselves.

## App Generator
Files in the extension `menu.json` can be loaded with the [JSONEditor4Menu](https://niehausbert.gitlab.io/jsoneditor4menu). You can preview the WebApp with [JSONEditor4Menu](https://niehausbert.gitlab.io/jsoneditor4menu) and generate a fully functional AppLSAC as a ZIP file with all files, that can be used for further development in context of the [learning resource in Wikiversity](https://en.wikiversity.org/wiki/AppLSAC).

## References to Repositories with AppLSACs
Some generated repositories on GitHub or GitLab are referenced here, so that you can fork them download them or explore them in the context of the [AppLSAC learning resource](https://en.wikiversity.org/wiki/AppLSAC).

Explore existing AppLSAC software that support the development of Wikiversity Learning Resources

### Wiki2Reveal
[Wiki2Reveal](https://niebert.github.io/Wiki2Reveal) is an AppLSAC that downloads a Wikiversity article and converts the article into a RevealJS or DZSlides presentation. Wiki2Reveal is an [AppLSAC-0](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Types_of_AppLSAC)

### Hamburger Menu App
'''[AppLSAC-2 with Navigation Menu](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Navigation_Menu):''' the first GitHub example [`hamburger_menu_app`](https://niebert.github.io/hamburger_menu_app) shows how to create a menu in an HTML web page. If you want to create your own see [JSONEditor4Menu](https://niehausbert.github.io/jsoneditor4menu).


### Image Map Editor
**[Image Map Editor](https://niebert.github.io/imgmap)** for [Image Maps](https://en.wikiversity.org/wiki/Image_Map) used in Wikiversity [(AppLSAC-1)](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Types_of_AppLSAC).
This AppLSAC does the image map editing totally in the browser, but it able to load images from the [Wiki Commons servers](https://commons.wikimedia.org/wiki/Main_Page).

### Audioslides4Web
**[Audioslides4Web](https://en.wikiversity.org/wiki/Audioslides4Web)** Create a web based presentation with MP3 audio comments and PNG slides - [AppLSAC-2](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Types_of_AppLSAC)

If you download the WebApp you can run the HTML5 application offline on your device with a browser as runtime environment as [(AppLSAC-2)](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Types_of_AppLSAC)

### JSON3D4Aframe - 3D Modelling
'''[https://niebert.github.io/JSON3D4Aframe JSON3D4Aframe]''' used for [3D Modelling/Create 3D Models](https://en.wikiversity.org/wiki/3D Modelling/Create_3D_Models)  - [AppLSAC-2](https://en.wikiversity.org/wiki/WebApps_with_LocalStorage_and_AppCache/Types_of_AppLSAC)
 
## AppLSAC Demo Files for Hamburger Menu Creator
Demo files scanned with `scan_demos4readme.sh` - Last Update: 20.10.2019
Generated file `./README.md` was included into `README.md` after calling `npm run build` 
The followig list contains all demo files for [AppLSAC]() as learning resources for [AppLSAC in Wikiversity](https://en.wikiversity.org/wiki/AppLSAC)
 
### editorsimple_menu.json 
The JSON file [editorsimple_menu.json](https://niehausbert.gitlab.io/applsac/hamburger-menu-creator/editorsimple_menu.json) can be used with the JSON editor [Hamburger Menu Creator](https://niebert.github.io/hamburger-menu-creator).
* Scanned 20.10.2019
 
## Wikiversity 
For further information see [Wikiversity](https://en.wikiversity.org/wiki/AppLSAC)
 
