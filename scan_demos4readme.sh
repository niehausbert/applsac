#!/bin/bash

SED_CMD="sed"
ROOT="./"
SCANPATH="hamburger-menu-creator/"
MODULE="AppLSAC"
DOMAIN="https://niehausbert.gitlab.io/applsac/"
WIKIVERSITY="https://en.wikiversity.org/wiki/AppLSAC"
OUTPUT="./README.md"
NOW=$(date +"%d.%m.%Y")
EDITORLINK="https://niebert.github.io/hamburger-menu-creator"
EDITORNAME="Hamburger Menu Creator"
### sed command - sed differs on OSX
### if you want to use this script on OSX install GNU sed with "brew install gsed"
### OSX
SED_CMD="gsed"
cp src/readme/README_header.md $OUTPUT
echo " " >> $OUTPUT
echo "## $MODULE Demo Files for $EDITORNAME" >> $OUTPUT
echo "Demo files scanned with \`scan_demos4readme.sh\` - Last Update: ${NOW}" >> $OUTPUT
echo "Generated file \`${OUTPUT}\` was included into \`README.md\` after calling \`npm run build\` " >> $OUTPUT
# echo "This file contains a list of \`*_menu.json\` files that can be loaded with <a href=\"$EDITORLINK\" target=\"_blank\">$EDITORNAME</a>."
# echo "The followig list contains all demo files for <a href=\"$REPO\" target=\"_blank\">$MODULE</a> as learning resources for <a href=\"$WIKIVERSITY\" target=\"_blank\">AppLSAC in Wikiversity</a>." >> $OUTPUT
echo "This file contains a list of \`*_menu.json\` files that can be loaded with [$EDITORNAME]($EDITORLINK)."
echo "The followig list contains all demo files for [$MODULE]($REPO) as learning resources for [AppLSAC in Wikiversity]($WIKIVERSITY)" >> $OUTPUT
echo " " >> $OUTPUT

i=0
filepath="$ROOT$SCANPATH"
#for filepath in `find "$ROOT" -maxdepth 1 -mindepth 1 -type d| sort`; do
#  path=`basename "$filepath"`
#  if [ "$path" = ".git" ]
#	then
#	echo "WARNING: Ignore '.git' folder for $OUTPUT"
#  else
#  	echo "DIR: $path"
#  	echo "  <LI><b><a href='$DOMAIN/wiki/$path' target='_blank'>$path</a></b></LI>" >> $OUTPUT
  	echo "Filepath: $filepath"
  	rm "${filepath}/.DS_Store"
  	for i in `find "$filepath" -maxdepth 1 -mindepth 1 -type f| sort`; do
    	file=`basename "$i"`
    	if [ "$file" = "index.html" ]
			then
			echo "- WARNING: Ignore self-reference to 'index.html' file for $OUTPUT"
  		else
        # title=`cat $i | $SED_CMD -n 's/"appname": "\(.*\)"/\1/Ip' | $SED_CMD -e 's/^[ \t]*//'`
          #title=`cat $i | $SED_CMD -n 's/appname?: ?\(.*\)\"/\1/Ip'
          # title=`cat $i | $SED_CMD -n 's/\"appname\": \"\(.*\)"/\1/Ip' | $SED_CMD -e 's/^[ \t]*//'`
          # comment=`cat $i | $SED_CMD -n 's/\"comment\": \"\(.*\)\"/\1/Ip'
          # generated=`cat $i | $SED_CMD -n 's/"generated": "\(.*\)\"/\1/Ip'
          title="$file"
          ## GNU: cat docs/index.html | sed -n 's/<title>\(.*\)<\/title>/\1/Ip'`
        	## OSX: cat docs/index.html | gsed -n 's/<title>\(.*\)<\/title>/\1/Ip'`
          echo "- FILE:  '$file'"
        	echo "  TITLE: '$title' "
          echo "### $title " >> $OUTPUT
      	  echo "The JSON file [$title](${DOMAIN}$SCANPATH$file) can be used with the JSON editor [$EDITORNAME]($EDITORLINK)." >> $OUTPUT
          echo "* Scanned $NOW" >> $OUTPUT
        fi
  	done
#  fi
# done
echo " " >> $OUTPUT
echo "## Wikiversity " >> $OUTPUT
echo "For further information see [Wikiversity](${WIKIVERSITY})" >> $OUTPUT
echo " " >> $OUTPUT
